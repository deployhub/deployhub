import re

from django.conf import settings
from django.conf.urls import include, patterns, url
from django.views.static import serve as serve_static

from rest_framework.routers import DefaultRouter

from deployhub.apps.account.api import views as account_views
from deployhub.apps.deploys.api import views as deploy_views
from deployhub.apps.repos.api import views as repo_views
from deployhub.apps.settings.views import settings_for_repo
from deployhub.apps.settings.api import views as settings_views

router = DefaultRouter()
router.register(r'accounts', account_views.AccountViewSet)
router.register(r'deploys', deploy_views.DeployViewSet)
router.register(r'repos', repo_views.RepositoryViewSet)


urlpatterns = patterns(
    '',

    url(r'^api/', include(router.urls)),
    url(r'^api/settings/(?P<uuid>[^/.]+)/$',
        settings_for_repo),
    url(r'^connect/', include('deployhub.apps.connect.urls')),
    url(r'^deploys/', include('deployhub.apps.deploys.urls')),
    url(r'^%s(?P<path>.*)$' % re.escape(settings.STATIC_URL.lstrip('/')),
        serve_static, kwargs={'document_root': settings.STATIC_ROOT}),
)
