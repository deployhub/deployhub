import json

from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.http.response import HttpResponseBadRequest
from django.views.decorators.http import require_GET, require_POST
from django.views.decorators.csrf import csrf_exempt

from deployhub.apps.connect.decorators import bitbucket_authenticated_request
from deployhub.apps.connect.models import BitbucketConnection
from deployhub.apps.connect.signals import (post_addon_installed,
                                            post_addon_uninstalled)

@require_GET
def connect_descriptor(request):
    descriptor = {
        "baseUrl": "{protocol}://{host}".format(
            protocol='https' if request.is_secure() else 'http',
            host=getattr(settings, "BASE_HOST", request.get_host())
        ),
        "name": "Deployhub",
        "description": "Bitbucket Deployment summaries",
        "key": "deployhub",
        "vendor": {
            "name": "Atlassian Inc.",
            "url": "http://www.atlassian.com"
        },
        "authentication": {
            "type": "jwt"
        },
        "scopes": ["repository"],
        "lifecycle": {
            "installed": reverse('connect.lifecycle.install'),
            "uninstalled": reverse('connect.lifecycle.uninstall'),
        },
        "modules": {
            "repoPage": [
                {
                    "url": "/deploys/{repo_uuid}/overview",
                    "name": {
                        "value": "Deploys",
                    },
                    "key": "deployhub-repository-overview",
                    "params": {
                        "auiIcon": "aui-iconfont-success"
                    },
                    "location": "org.bitbucket.repository.navigation"
                }, {
                    "url": "/deploys/{repo_uuid}/create",
                    "name": {
                        "value": "Create Deploy",
                    },
                    "key": "deployhub-repository-overview",
                    "params": {
                        "auiIcon": "aui-iconfont-deploy"
                    },
                    "location": "org.bitbucket.repository.actions"
                }
            ],

            "adminPage": [{
                "url": "/deploys/{repo_uuid}/settings",
                "name": {
                    "value": "DeployHub Settings",
                    },
                "key": "deployhub-repository-settings",

                "params": {
                    "auiIcon": "aui-iconfont-configure"
                },
                "location": "org.bitbucket.repository.admin"
            }]
        }
    }
    return HttpResponse(json.dumps(descriptor),
                        content_type='application/json')


@require_POST
@csrf_exempt
@bitbucket_authenticated_request(required=False)
def lifecycle_install_event(request):
    event = json.loads(request.body.decode('utf-8'))

    if event['eventType'] != 'installed':
        return HttpResponseBadRequest()

    connection = BitbucketConnection.install(
        client_key=event['clientKey'], shared_secret=event['sharedSecret'],
        bb_url=event['baseUrl']
    )

    post_addon_installed.send(sender=lifecycle_install_event,
                              connection=connection)
    return HttpResponse(status=204)


@require_POST
@csrf_exempt
@bitbucket_authenticated_request(required=False)
def lifecycle_uninstall_event(request):
    event = json.loads(request.body.decode('utf-8'))

    if event['eventType'] != 'uninstalled':
        return HttpResponseBadRequest()

    if not request.bb:
        return HttpResponseBadRequest()
    request.bb.delete()

    post_addon_uninstalled.send(sender=lifecycle_uninstall_event,
                                connection=request.bb)

    return HttpResponse(status=204)
