from functools import wraps
from django.http.response import HttpResponseBadRequest

from deployhub.apps.connect.models import BitbucketConnection


def bitbucket_authenticated_request(*args, **kwargs):
    parametrized = len(args) != 1 or not callable(args[0])
    required = not parametrized or kwargs.get('required', True)

    def wrapper(func):
        @wraps(func)
        def wrapped(request, *args, **kwargs):
            headers = getattr(request, 'META', {})
            if ('HTTP_AUTHORIZATION' in headers and
                    'JWT' in headers['HTTP_AUTHORIZATION']):
                token = headers['HTTP_AUTHORIZATION'][4:]
                request.bb = BitbucketConnection.connection_from_jwt(token)
            elif request.method == 'GET':
                token = request.GET.get('jwt')
                request.bb = BitbucketConnection.connection_from_jwt(token)
            else:
                if required:
                    return HttpResponseBadRequest()
                else:
                    request.bb = None
            return func(request, *args, **kwargs)
        return wrapped
    return wrapper if parametrized else wrapper(args[0])
