import json
from hashlib import sha256
from time import time

try:
    from urllib.parse import urlencode, urljoin
except ImportError:
    from urllib import urlencode
    from urlparse import urljoin

import jwt
import requests
from django.db import models

from deployhub.apps.account.models import Account


def _hash_url(method, url):
    """Hash URL based on Connect hashing specification"""
    url = url or '/'
    qs = []
    if '?' in url:
        url, qs = url.split('?')
        qs = sorted(qs.split('&'))

    result = u'%s&%s&%s' % (method.upper(), url, '&'.join(qs))
    sha = sha256(result.encode('utf-8')).hexdigest()
    return sha


class BitbucketHost(models.Model):
    base_url = models.CharField(max_length=64)


class BitbucketConnection(models.Model):
    host = models.ForeignKey(BitbucketHost)
    shared_secret = models.CharField(max_length=128)
    client_key = models.CharField(max_length=128, db_index=True)
    account = models.OneToOneField(Account, related_name='connection')

    class Meta:
        unique_together = ('host', 'client_key')

    def create_token(self, url, method='POST', timeout=3600000):
        payload = {'iss': 'deployhub', 'iat': int(time()),
                   'exp': int(time()) + timeout, 'sub': self.client_key,
                   'qsh': _hash_url(method.upper(), url)}
        return jwt.encode(payload=payload, key=self.shared_secret,
                          algorithm='HS256').decode('ascii')

    @classmethod
    def install(cls, client_key, shared_secret,
                bb_url='https://bitbucket.org'):
        """Creates or updates a Bitbucket connection.

        If a Bitbucket connection already been established for the supplied
        client_key or a connection has already been established for the account
        owner that connection will be updated.
        """
        try:
            # allow the service to refresh our information for a connection
            connection = cls.objects.select_related('account').get(
                host__base_url=bb_url, client_key=client_key
            )
        except cls.DoesNotExist:
            host, created = BitbucketHost.objects.get_or_create(
                base_url=bb_url
            )
            connection = cls(host=host, client_key=client_key)

        connection.shared_secret = shared_secret
        user_info = connection.user_info()

        try:
            account = Account.objects.get(uuid=user_info['uuid'])
            account.username = user_info['username']
            account.display_name = user_info['display_name']
        except Account.DoesNotExist:
            account = Account.objects.create(
                username=user_info['username'], uuid=user_info['uuid'],
                display_name=user_info['display_name'],
                acc_type=user_info['type']
            )
        account.save()
        connection.account = account

        try:
            existing = BitbucketConnection.objects.get(
                account__uuid=user_info['uuid']
            )
        except BitbucketConnection.DoesNotExist:
            pass
        else:
            # Only allow the plugin to be installed once per user (or team)
            # if it gets installed again for some reason, set the id of this
            # connection to that existing connection so when the connection is
            # saved the original connection is updated.
            connection.id = existing.id

        connection.save()

        return connection

    @classmethod
    def connection_from_jwt(cls, token):
        payload = jwt.decode(token, verify=False)
        connection = cls.objects.get(client_key=(payload['iss']))
        jwt.decode(token, connection.shared_secret)
        return connection

    def _authorization_header(self, url):
        token = self.create_token(url)
        return {'Authorization': 'JWT %s' % token}

    def send_request(self, url, params=None, raise_for_status=True, parse=True):
        url = urljoin(self.host.base_url, url)

        resp = requests.get(url, headers=self._authorization_header(url),
                            params=params)
        if raise_for_status:
            resp.raise_for_status()

        if parse:
            return json.loads(resp.text)
        else:
            return resp

    def user_info(self):
        return self.send_request('api/2.0/user')
