# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='BitbucketConnection',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('shared_secret', models.CharField(max_length=128)),
                ('client_key', models.CharField(max_length=128, db_index=True)),
                ('account', models.OneToOneField(related_name='connection', to='account.Account')),
            ],
        ),
        migrations.CreateModel(
            name='BitbucketHost',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('base_url', models.CharField(max_length=64)),
            ],
        ),
        migrations.AddField(
            model_name='bitbucketconnection',
            name='host',
            field=models.ForeignKey(to='connect.BitbucketHost'),
        ),
        migrations.AlterUniqueTogether(
            name='bitbucketconnection',
            unique_together=set([('host', 'client_key')]),
        ),
    ]
