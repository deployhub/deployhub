from django.dispatch import Signal


post_addon_installed = Signal(providing_args=['connection'])

post_addon_uninstalled = Signal(providing_args=['connection'])
