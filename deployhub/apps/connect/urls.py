from django.conf.urls import include, patterns, url
from deployhub.apps.connect import views

urlpatterns = patterns(
    '',

    url(r'^descriptor.json$', views.connect_descriptor),
    url(r'^lifecycle/install$', views.lifecycle_install_event,
        name='connect.lifecycle.install'),
    url(r'^lifecycle/uninstall$', views.lifecycle_uninstall_event,
        name='connect.lifecycle.uninstall'),
)
