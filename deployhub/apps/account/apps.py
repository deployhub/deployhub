from django.apps import AppConfig


class AccountAppConfig(AppConfig):
    name = 'deployhub.apps.account'
    verbose_name = 'Account'

    def ready(self):
        # The import causes the signal receivers to be registered
        from deployhub.apps.account import signals
