from django.dispatch import receiver

from deployhub.apps.account.tasks import index_account
from deployhub.apps.connect.signals import post_addon_installed


@receiver(post_addon_installed, dispatch_uid='post_install_account_indexing')
def trigger_post_install_account_indexing(sender, **kwargs):
    connection = kwargs['connection']
    index_account.apply_async((connection.id,))
