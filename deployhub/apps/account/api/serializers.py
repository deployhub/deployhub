from rest_framework.serializers import HyperlinkedModelSerializer

from deployhub.apps.account.models import Account


class AccountSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Account
        lookup_field = 'uuid'
