from rest_framework import viewsets

from deployhub.apps.account.api.serializers import AccountSerializer
from deployhub.apps.account.models import Account


class AccountViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Account.objects.all()
    serializer_class = AccountSerializer
    lookup_field = 'uuid'
