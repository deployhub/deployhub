# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('username', models.CharField(unique=True, max_length=30, db_index=True)),
                ('uuid', models.UUIDField(unique=True, editable=False, db_index=True)),
                ('display_name', models.CharField(max_length=100, blank=True)),
                ('acc_type', models.CharField(max_length=16, choices=[(b'user', b'User'), (b'team', b'Team')])),
            ],
        ),
    ]
