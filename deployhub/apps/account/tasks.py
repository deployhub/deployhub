from deployhub.apps.connect.models import BitbucketConnection
from deployhub.apps.repos.models import Repository  # , Branch
from deployhub.celery import app


@app.task
def index_account(connection_id):
    """Indexes an account.

    This is a celery task that gets all repositories belonging to a Bitbucket
    account and then proceeds to pull down all the commits in each of
    those repositories. This task is typically initiated during the
    installation of the add-on on an account.
    """
    connection = BitbucketConnection.objects.select_related('account').get(
        id=connection_id
    )
    upstream_repos = connection.account.upstream_repos()

    # In case there are already some repos attached to the client, we need to
    # purge them
    connection.account.repos.all().delete()

    # Now map the upstream_repos to the Repository model
    for repo in upstream_repos:
        Repository.objects.create(
            owner=connection.account, name=repo['name'], scm=repo['scm'],
            uuid=repo['uuid'],
        )
