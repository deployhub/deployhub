from django.db import models


ACCOUNT_TYPE_CHOICES = (
    ('user', 'User'),
    ('team', 'Team'),
)


class Account(models.Model):
    username = models.CharField(max_length=30, unique=True, db_index=True)
    uuid = models.UUIDField(unique=True, db_index=True, editable=False)
    display_name = models.CharField(max_length=100, blank=True)
    acc_type = models.CharField(max_length=16, choices=ACCOUNT_TYPE_CHOICES)

    def upstream_repos(self):
        repos = []
        url_format = ('api/2.0/repositories/{{{uuid}}}?pagelen=100&fields=-values.owner,-values.links')
        next_url = url_format.format(uuid=self.uuid)

        while next_url:
            resp = self.connection.send_request(next_url)
            incoming_repos = resp['values']
            
            repos.extend(incoming_repos)
            next_url = None
            # next_url = resp.get('next')

        return repos
