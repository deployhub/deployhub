from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views.decorators.http import require_http_methods
from rest_framework.renderers import JSONRenderer
from deployhub.apps.settings.api.serializers import SettingsSerializer
from deployhub.apps.settings.models import Settings
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
from rest_framework.decorators import api_view

# TODO: fix this
class JSONResponse(HttpResponse):
    """
    An HttpResponse that renders its content into JSON.
    """
    def __init__(self, data, **kwargs):
        content = JSONRenderer().render(data)
        kwargs['content_type'] = 'application/json'
        super(JSONResponse, self).__init__(content, **kwargs)

@api_view(['GET', 'POST'])
@require_http_methods(["GET", "POST"])
def settings_for_repo(request, uuid):
    if request.method == "GET":
        settings = get_object_or_404(Settings, repo__uuid=uuid)
        return JSONResponse(
            SettingsSerializer(settings, context={'request': request}).data
        )

    if request.method == "POST":
        data = JSONParser().parse(request)
        serializer = SettingsSerializer(data=data, context={'request': request})
        if serializer.is_valid():
            serializer.save()
            return JSONResponse(serializer.data, status=201)

        return JSONResponse(serializer.errors, status=400)

