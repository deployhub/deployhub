from django.db import models

from deployhub.apps.repos.models import Repository


class Settings(models.Model):
    repo = models.OneToOneField(Repository, unique=True)
    root_hash = models.CharField(max_length=40)
