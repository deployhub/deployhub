from rest_framework import viewsets, mixins
from rest_framework.decorators import detail_route

from deployhub.apps.repos.api.serializers import RepositorySerializer
from deployhub.apps.repos.models import Repository
from deployhub.apps.settings.api.serializers import SettingsSerializer
from deployhub.apps.settings.models import Settings


class SettingsViewSet(viewsets.ModelViewSet):
    queryset = Settings.objects.all()
    serializer_class = SettingsSerializer

