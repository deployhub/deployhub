import collections

from rest_framework.relations import HyperlinkedRelatedField
from rest_framework.serializers import HyperlinkedModelSerializer

from deployhub.apps.repos.models import Repository
from deployhub.apps.settings.models import Settings


class SettingsSerializer(HyperlinkedModelSerializer):
    repo = HyperlinkedRelatedField(read_only=True,
                                   view_name='repository-detail',
                                   lookup_field='uuid')

    class Meta:
        model = Settings
        fields = ('root_hash', 'repo',)

    def __init__(self, *args, **kwargs):
        super(SettingsSerializer, self).__init__(*args, **kwargs)

    def create(self, validated_data):
        repo = Repository.objects.get(uuid=self.initial_data['repo_id'])

        settings = Settings.objects.filter(repo=repo).first()

        if settings:
            settings.root_hash = validated_data['root_hash']
            settings.save()
        else:
            settings = Settings.objects.create(
                repo=repo,
                root_hash=validated_data['root_hash']
            )

        return settings

    def update(self, instance, validated_data):
        return super(SettingsSerializer, self).update(instance, validated_data)