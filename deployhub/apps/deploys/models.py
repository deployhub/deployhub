from django.db import models

from deployhub.apps.repos.models import Repository


class Deployment(models.Model):
    repo = models.ForeignKey(Repository, related_name='deploys')
    hash = models.CharField(max_length=40)
    creation_timestamp = models.DateTimeField(auto_now_add=True)
    update_timestamp = models.DateTimeField(auto_now=True)
    description = models.TextField(blank=True, null=True)
