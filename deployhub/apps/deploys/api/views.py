from rest_framework import viewsets

from deployhub.apps.deploys.api.serializers import DeploySerializer
from deployhub.apps.deploys.models import Deployment


class DeployViewSet(viewsets.ModelViewSet):
    queryset = Deployment.objects.all().order_by('-creation_timestamp')
    serializer_class = DeploySerializer
