from django.core.exceptions import ObjectDoesNotExist
from rest_framework.fields import SerializerMethodField
from rest_framework.relations import HyperlinkedRelatedField
from rest_framework.serializers import HyperlinkedModelSerializer

from deployhub.apps.deploys.models import Deployment
from deployhub.apps.repos.models import Repository


class DeploySerializer(HyperlinkedModelSerializer):
    repo = HyperlinkedRelatedField(read_only=True,
                                               view_name='repository-detail',
                                               lookup_field='uuid')
    prev_hash = SerializerMethodField()

    class Meta:
        model = Deployment

    def __init__(self, *args, **kwargs):
        nested = kwargs.pop('nested', False)
        super(DeploySerializer, self).__init__(*args, **kwargs)
        if nested:
            self.fields.pop('repo')

    def create(self, validated_data):
        # TODO: I'm pretty sure this is a hack-job.
        data = self.context['request'].data
        repo = Repository.objects.get(uuid=data['repo']['uuid'])
        deployment = Deployment.objects.create(
            repo=repo,
            hash=validated_data['hash'],
            description=validated_data['description']
        )
        return deployment

    def update(self, instance, validated_data):
        validated_data.pop('repo')
        return super(DeploySerializer, self).update(instance, validated_data)

    def get_prev_hash(self, deploy):
        prev_dep = Deployment.objects.filter(
            repo_id=deploy.repo_id,
            creation_timestamp__lt=deploy.creation_timestamp
        ).order_by('-creation_timestamp').first()

        if prev_dep:
            return prev_dep.hash
        else:
            try:
                return deploy.repo.settings.root_hash
            except ObjectDoesNotExist:
                return None
