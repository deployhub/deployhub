from django.conf.urls import patterns, url


urlpatterns = patterns(
    'deployhub.apps.deploys.views',

    url(r'^healthcheck/?', 'healthcheck',
        name='deploys.healthcheck'),

    url(r'^(?P<repo_uuid>[\w\d-]+)/(?P<page_name>overview|create|settings)', 'show_react',
        name='deploys.show_react'),
)
