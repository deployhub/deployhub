from django.http import HttpResponse
from django.shortcuts import render_to_response

from deployhub.apps.connect.decorators import bitbucket_authenticated_request
from deployhub.apps.connect.models import BitbucketConnection
from deployhub.apps.repos.models import Repository


def healthcheck(request):
    """Respond to health check from Micros"""
    return HttpResponse(status=200)

@bitbucket_authenticated_request
def show_react(request, repo_uuid, page_name):
    return render_to_response('index.html', {
        'bb_base_url': request.bb.host.base_url
    })
