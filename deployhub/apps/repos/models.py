from dateutil import parser as iso8601parser
from django.db import models

from deployhub.apps.account.models import Account


SCM_CHOICES = (
    ('git', 'Git'),
    ('hg', 'Mercurial'),
)


class Repository(models.Model):
    owner = models.ForeignKey(Account, related_name='repos')
    name = models.CharField(max_length=255)
    scm = models.CharField(max_length=64, choices=SCM_CHOICES)
    uuid = models.UUIDField(unique=True, db_index=True, editable=False)
