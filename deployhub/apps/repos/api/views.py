from rest_framework import viewsets, mixins
from rest_framework.decorators import detail_route

from deployhub.apps.repos.api.serializers import RepositorySerializer
from deployhub.apps.repos.models import Repository


class RepositoryViewSet(mixins.RetrieveModelMixin,
                        mixins.UpdateModelMixin,
                        mixins.ListModelMixin,
                        viewsets.GenericViewSet):
    queryset = Repository.objects.all()
    serializer_class = RepositorySerializer
    lookup_field = 'uuid'
