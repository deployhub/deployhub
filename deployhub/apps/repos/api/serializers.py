import collections

from rest_framework.serializers import HyperlinkedModelSerializer

from deployhub.apps.account.api.serializers import AccountSerializer
from deployhub.apps.deploys.api.serializers import DeploySerializer
from deployhub.apps.repos.models import Repository
from deployhub.apps.settings.api.serializers import SettingsSerializer


class RepositorySerializer(HyperlinkedModelSerializer):
    owner = AccountSerializer(read_only=True)
    deploys = DeploySerializer(many=True, read_only=True, nested=True)
    settings = SettingsSerializer(read_only=True)

    class Meta:
        model = Repository
        read_only_fields = ('name', 'scm', 'uuid')
        lookup_field = 'uuid'

    def __init__(self, *args, **kwargs):
        super(RepositorySerializer, self).__init__(*args, **kwargs)
        if isinstance(self.instance, collections.Iterable):
            self.fields.pop('deploys')
