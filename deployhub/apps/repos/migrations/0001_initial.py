# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Repository',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('scm', models.CharField(max_length=64, choices=[(b'git', b'Git'), (b'hg', b'Mercurial')])),
                ('uuid', models.UUIDField(unique=True, editable=False, db_index=True)),
                ('root_hash', models.CharField(max_length=40, null=True, blank=True)),
                ('owner', models.ForeignKey(related_name='repos', to='account.Account')),
            ],
        ),
    ]
