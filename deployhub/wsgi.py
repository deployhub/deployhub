import os
import sys

here = os.path.dirname(os.path.abspath(__file__))
root = os.path.join(here, '../')

sys.path.insert(0, root)

if 'DEPLOYHUB_ENV' in os.environ:
    if os.environ.get('DEPLOYHUB_ENV') == 'production':
        os.environ['DJANGO_SETTINGS_MODULE'] = 'deployhub.settings.deploy'

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'deployhub.settings.deploy')

os.environ['PYTHON_EGG_CACHE'] = '/tmp/python-eggs'
os.environ['LANG'] = 'en_US.UTF-8'
os.environ["CELERY_LOADER"] = "django"

from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
