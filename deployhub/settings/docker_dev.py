# Base settings
from .dev import *

DATABASES['default'] = {
    'ENGINE': 'django.db.backends.postgresql_psycopg2',
    'HOST': 'db',
    'NAME': 'deployhub',
    'USER': 'deployhub',
    'PORT': 5432
}

STATIC_ROOT = os.path.join('/usr', 'local', 'lib', 'frontend')

