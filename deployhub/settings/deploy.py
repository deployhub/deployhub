from .base import *
import os

# import dj_database_url
# DATABASES['default'] = dj_database_url.config()

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

DEBUG = TEMPLATE_DEBUG = False

# this ends up being the URL being passed to BB
# against which BB will invoke a callback
if os.environ.get('ENV') == 'production' and os.environ.get('DEPLOYHUB_BASE_HOST'):
    BASE_HOST = os.environ.get('DEPLOYHUB_BASE_HOST')

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'deployhub',
        'USER': 'deployhub',
        'PASSWORD': 'deployhub',
    }
}

# Celery
BROKER_URL = 'amqp://guest:guest@localhost:5672//'
BROKER_POOL_LIMIT = 1
