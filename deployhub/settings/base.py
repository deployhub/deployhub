import os
import json
SETTINGS_DIR = os.path.abspath((os.path.dirname(__file__)))
BASE_DIR = os.path.join(SETTINGS_DIR, '..')
FRONTEND_DIR = os.path.join(BASE_DIR, '..', 'frontend')

with open(os.path.join(FRONTEND_DIR, 'src', 'settings', 'base.json')) as frontendJSON:
    frontend = json.load(frontendJSON)

SECRET_KEY = '-l^w_$_ib9o89+0(b@9q3s4&d431#*6m(jso-9i*s%+n-ainb@'

DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = ['*']

# Application definition
INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'rest_framework',

    'deployhub.apps.account',
    'deployhub.apps.connect',
    'deployhub.apps.deploys',
    'deployhub.apps.repos',
    'deployhub.apps.settings',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',

    # TODO: Use a better approach that only allows bitbucket.org to use this
    # add-on in a frame
    # 'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'deployhub.urls'
WSGI_APPLICATION = 'deployhub.wsgi.application'

# Databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'deployhub',
        'USER': 'deployhub',
    }
}

# Celery
BROKER_URL = 'amqp://guest:guest@localhost:5672//'
CELERY_ACCEPT_CONTENT = ['json']
CELERY_DEFAULT_EXCHANGE = "task"
CELERY_DEFAULT_EXCHANGE_TYPE = "topic"
CELERY_DEFAULT_ROUTING_KEY = "task.regular"
CELERY_DEFAULT_DELIVERY_MODE = "persistent"
CELERY_TASK_SERIALIZER = 'json'
CELERY_RESULT_SERIALIZER = 'json'

# Internationalization
LANGUAGE_CODE = 'en-us'
TIME_ZONE = 'UTC'
USE_I18N = False
USE_L10N = False
USE_TZ = True

# Static files
STATIC_URL = frontend['STATIC_URL']
STATIC_ROOT = os.path.join(FRONTEND_DIR, 'build')

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)

# Templates
TEMPLATE_DIRS = (os.path.join(BASE_DIR, 'templates'),)

# Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': ('%(levelname)s %(asctime)s %(module)s %(process)d '
                       '%(thread)d %(message)s')
        }
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
    },
    'loggers': {
        'django.request': {
            'handlers': ['console'],
            'level': 'DEBUG',
            'propagate': False,
        }
    }
}

CELERY_ALWAYS_EAGER = True
CELERY_EAGER_PROPAGATES_EXCEPTIONS = True
