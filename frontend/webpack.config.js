var path = require('path');
var webpack = require('webpack');
var root = path.join(__dirname, 'src');

var outputPath = process.env.NODE_ENV === 'production' ?
  path.join(__dirname, 'build') :
  path.join(__dirname, 'static');

module.exports = {
  context: root,
  entry: './index',
  devtool: 'sourcemap',

  output: {
      path: outputPath,
      filename: 'app.js'
  },

  resolve: {
    modulesDirectories: ['node_modules'],
    extensions: ['', '.js', '.jsx', '.json']
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': '"production"'
      }
    })
  ],

  module: {
    loaders: [
      { test: /\.jsx?$/, exclude: /node_modules/, loaders: ['babel-loader', 'envify-loader']},
      { test: /\.json/, loader: 'json-loader'},
      { test: /\.css$/, loader: 'style-loader!css-loader' }
    ]
  }

}
