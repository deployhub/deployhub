import base from './base';
import assign from 'lodash.assign';

// This is intentionally simple so webpack can statically
// remove unused configs
var additional = {};
if (process.env.NODE_ENV === 'production') {
  additional = require('./deploy.json');
}

export default assign({}, base, additional);
