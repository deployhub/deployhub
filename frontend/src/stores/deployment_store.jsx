import Reflux from 'reflux';
import settings from '../settings';

export var DeploymentActions = Reflux.createActions(['load']);

function daysAgo(n) {
  var date = new Date();
  date.setDate(date.getDate() - n);
  return date;
}

var getAsync = function(repoId) {
  // TODO: can potentially cache some data here,
  // assuming cache-busting isn't prohibitively complex

  var endpoint = `${settings.API_ROOT}api/deploys/`;

  return new Promise(function (resolve, reject) {
    $.getJSON(endpoint).done(function (results) {
      var matches = results.filter(function (record) {
        return record.repo.indexOf(repoId) >= 0;
      }).map(function (record) {
        record.creation_timestamp = new Date(record.creation_timestamp);
        return record;
      });

      resolve(matches);
    }, reject);
  }).catch(function(err) {
    console.error('err', err);
  });
}

export var DeploymentStore = Reflux.createStore({
  listenables: DeploymentActions,

  onLoad: function(repoId) {
    getAsync(repoId).then((result) => {
      this.trigger(result);
    }, function (err) {
      console.error(err);
    });
  }
});
