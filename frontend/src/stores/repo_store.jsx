import Reflux from 'reflux';
import settings from '../settings';
import _ from 'lodash';

export var RepoActions = Reflux.createActions(['load']);

var getAsync = function(repoId) {
  // cache repoId stuff here
  var endpoint = `${settings.API_ROOT}api/repos/${repoId}/`;

  return new Promise(function (resolve, reject) {
    $.getJSON(endpoint).then(function (result) {
      result.repoSlug = "{" + result.owner.uuid + '}/{' + result.uuid + '}';
      resolve(result);
    }, reject);
  }).catch(function(err) {
    console.error('err', err);
  });
}

export var RepoStore = Reflux.createStore({
  listenables: RepoActions,

  init: function() {
    this.listenTo(RepoActions.load, this.onLoad);
  },
  onLoad: function(repoId) {
    getAsync(repoId).then((result) => {
      this.trigger(result);
    }, function (err) {
      console.error(err);
    });
  }
});
