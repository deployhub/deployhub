import Reflux from 'reflux';
import settings from '../settings';
import _ from 'lodash';

export var SettingsActions = Reflux.createActions(['load']);

var getAsync = function(repoId) {
  // cache repoId stuff here
  var endpoint = `${settings.API_ROOT}api/settings/${repoId}/`;

  return new Promise(function (resolve, reject) {
    $.getJSON(endpoint).then(resolve, reject);
  }).catch(function(err) {
      console.error('err', err);
      throw err;
    });
}

export var SettingsStore = Reflux.createStore({
  listenables: SettingsActions,

  init: function() {
    this.listenTo(SettingsActions.load, this.onLoad);
  },

  onLoad: function(repoId) {
    getAsync(repoId).then((result) => {
      this.trigger(result);
    }, (err) => {
      if (err.status === 404) {
        this.trigger({});
      } else {
        alert("OH NOES!");
      }
      console.error(err);
    });
  }
});
