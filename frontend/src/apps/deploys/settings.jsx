import forms from 'newforms';
import QueryString from 'query-string';
import React from 'react';
import renderField from '../../utils/renderField';
import Reflux from 'reflux';
import {RepoActions, RepoStore} from '../../stores/repo_store'
import Router from 'react-router';
import SettingsForm from '../../forms/settings-form'
import {SettingsActions, SettingsStore} from '../../stores/settings_store.jsx'
import LoadingSpinner from '../../components/loading-spinner'

var DeploySettings = React.createClass({
  displayName: 'DeploySettings',
  mixins: [
    Reflux.connect(RepoStore, 'repo')
  ],

  componentDidMount: function () {
    RepoActions.load(this.context.router.getCurrentParams().repoId);
    SettingsActions.load(this.context.router.getCurrentParams().repoId);
    this.listenTo(SettingsStore, this.onSettingsChange);
  },

  onSettingsChange: function(settings) {
    this.settingsChanged = true;
    this.setState({
      settings: settings
    });
  },

  componentWillMount: function () {
    this.form = new SettingsForm({
      onChange: this.onChange,
      forceReactUpdate: this.forceUpdate.bind(this),
      getRepo: () => {
        return this.state.repo
      }
    });
  },
  componentWillUpdate: function (_nextProps, nextState) {
    // this is more future-proof than checking for equality between
    //   nextState.settings and this.state.settings
    if (this.settingsChanged) {
      this.settingsChanged = false;

      this.form = new SettingsForm({
        onChange: this.onChange,
        forceReactUpdate: this.forceUpdate.bind(this),
        data: nextState.settings,
        errors: forms.ErrorObject.fromJSON({}),
        getRepo: () => {
          return this.state.repo
        }
      });
    }
  },

  onChange: function () {
    if (!this.form.isValid()) {
      this.forceUpdate();
    }
  },

  onSubmit: function (e) {
    e.preventDefault();

    var $submitButton = $(this.refs.submitButton.getDOMNode());
    var originalSubmitButtonValue = $submitButton.val();
    $submitButton.attr('value', 'Validating...');

    // synchronous validation
    this.form.validate(() => {
      if (!this.form.isValid()) {
        $submitButton.attr('value', originalSubmitButtonValue);
        this.forceUpdate();
      } else {
        $submitButton.attr('value', 'Submitting...');
        var data = $.extend(true, {
          repo_id: this.context.router.getCurrentParams().repoId
        }, this.form.cleanedData);

        $.ajax({
          //beforeSend: function (xhr, settings) {
          //  if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
              //xhr.setRequestHeader("X-CSRFToken", csrftoken);
            //}
          //},
          type: 'post',
          url: `/api/settings/${this.context.router.getCurrentParams().repoId}/`,
          contentType: 'application/json',
          data: JSON.stringify(data)
        }).done(() => {
          this.context.router.transitionTo('index', {
            repoId: this.context.router.getCurrentParams().repoId
          }, QueryString.parse(location.search));
        }).fail((res) => {
          // TODO: in a productionized version, we'd specifically handle expected errors
          alert("The save failed.");
        }).always(() => {
          if (!this.refs.submitButton) return;
          $(this.refs.submitButton.getDOMNode()).attr('value', originalSubmitButtonValue);
        });
      }
    });
  },

  render: function() {
    return <form className="aui" onSubmit={this.onSubmit}>
      {this.state.repo && this.state.settings ? (
        <fieldset>
          {this.form.boundFields().map(renderField)}
          <input ref="submitButton" className="button submit" type="submit" value="Submit" id="d-save-btn1" />
        </fieldset>
      ) : (
        <LoadingSpinner />
      )}
    </form>
  }
});

DeploySettings.contextTypes = {
  router: React.PropTypes.func
};

export default DeploySettings;
