import React from 'react';
import forms from 'newforms';
import CreateDeploymentForm from '../../forms/create-deployment-form';
import QueryString from 'query-string';
import Reflux from 'reflux';
import Router from 'react-router';
import {RepoActions, RepoStore} from '../../stores/repo_store';
import LoadingSpinner from '../../components/loading-spinner';
import renderField from '../../utils/renderField';

var CreateDeployment = React.createClass({
  mixins: [
    Reflux.connect(RepoStore, 'repo')
  ],

  componentWillMount: function () {
    this.form = new CreateDeploymentForm({
      onChange: this.onChange,
      forceReactUpdate: this.forceUpdate.bind(this),
      getRepo: () => {
        return this.state.repo
      }
    });
  },

  componentDidMount: function () {
    RepoActions.load(this.context.router.getCurrentParams().repoId);
  },

  /**
   * the forms lib automatically validates the input before calling
   *   onChange
   */
  onChange: function () {
    if (!this.form.isValid()) {
      this.forceUpdate();
    }
  },

  onSubmit: function (e) {
    e.preventDefault();

    var $submitButton = $(this.refs.submitButton.getDOMNode());
    var originalSubmitButtonValue = $submitButton.val();
    $submitButton.attr('value', 'Validating...');

    this.form.validate(() => {
      if (!this.form.isValid()) {
        $submitButton.attr('value', originalSubmitButtonValue);
        this.forceUpdate();
      } else {
        $(this.refs.submitButton.getDOMNode()).attr('value', "Submitting...");

        var data = $.extend(true, {
          repo: {
            uuid: this.context.router.getCurrentParams().repoId
          }
        }, this.form.cleanedData);

        $.ajax({
          type: 'post',
          url: '/api/deploys/',
          contentType: 'application/json',
          data: JSON.stringify(data)
        }).done(() => {
          this.context.router.transitionTo('index', {
            repoId: this.context.router.getCurrentParams().repoId
          }, QueryString.parse(location.search));
        }).fail((res) => {
          // TODO: in a productionized version, we'd specifically handle expected errors
          alert("The save failed.");
        }).always(() => {
          if (!this.refs.submitButton) return;
          $(this.refs.submitButton.getDOMNode()).attr('value', originalSubmitButtonValue);
        });
      }
    });
  },

  render: function () {
    return <form className="aui" onSubmit={this.onSubmit}>
      {this.state.repo ? (
        <fieldset>
          {this.form.boundFields().map(renderField)}
          <input ref="submitButton" className="button submit" type="submit" value="Submit" id="d-save-btn1" />
        </fieldset>
      ) : (
        <LoadingSpinner />
      )}
    </form>
  }
});

CreateDeployment.contextTypes = {
  router: React.PropTypes.func
};

export default CreateDeployment;
