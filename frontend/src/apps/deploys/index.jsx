import React from 'react';
import Reflux from 'reflux';
import Router from 'react-router';
import AUISortableTable from '../../components/aui-sortable-table';
import {DeploymentActions, DeploymentStore} from '../../stores/deployment_store'
import {RepoActions, RepoStore} from '../../stores/repo_store'
import LoadingSpinner from '../../components/loading-spinner'


class DeployRow extends React.Component {
  render() {
    var deploy = this.props.deploy;

    var baseUrl = window.DH.clientBaseUrl; // TODO: un-hardcode
    var url;
    var linkText;

    if (deploy.prev_hash) {
      url = baseUrl + this.props.repo.repoSlug + '/branches/compare/' + deploy.prev_hash + '..' + deploy.hash;
      linkText = deploy.prev_hash.substring(0, 7) + '..' + deploy.hash.substring(0, 7);
    } else {
      alert("We've noticed that you haven't configured your root hash yet. Check out the settings page.");
      // link directly to the commit if there is no previous hash.
      url = baseUrl + this.props.repo.repoSlug + '/commits/' + deploy.hash;
      linkText = deploy.hash.substring(0, 7);
    }

    return (
      <tr>
        <td><time>{deploy.creation_timestamp.toLocaleDateString()}, {deploy.creation_timestamp.toLocaleTimeString()}</time></td>
        <td>{deploy.prev_hash}</td>
        <td>{deploy.hash}</td>
        <td>{deploy.description}</td>
        <td><a target='_blank' href={url}>{linkText}</a></td>
      </tr>
    )
  }
}

let columnSpec = [
  {key: 'creation_timestamp', name: 'Created'},
  {key: 'prev_hash', name: 'Previous SHA'},
  {key: 'hash', name: 'SHA'},
  {key: 'description', name: 'Description'},
  {key: null, name: 'Compare'},
];

var DeployList = React.createClass({
  displayName: 'DeployList',
  mixins: [
    Reflux.connect(DeploymentStore, 'deploys'),
    Reflux.connect(RepoStore, 'repo')
  ],
  componentDidMount: function () {
    RepoActions.load(this.context.router.getCurrentParams().repoId);
    DeploymentActions.load(this.context.router.getCurrentParams().repoId);
  },
  genRow: function (record, i) {
    return <DeployRow deploy={record} key={i} repo={this.state.repo} />
  },
  render: function() {
    return (
      <div className='full-width relative'>
      {this.state.repo && this.state.deploys ? (
          <AUISortableTable columns={columnSpec} data={$.extend(true, [], this.state.deploys)} genRow={this.genRow.bind(this)} />
        ) : (
          <LoadingSpinner />
      )}
      </div>
    )
  }
});

DeployList.contextTypes = {
  router: React.PropTypes.func
};

export default DeployList;
