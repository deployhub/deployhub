import './index.css';

import React from 'react';
import Router from 'react-router';
import DeployList from './apps/deploys';
import DeployCreate from './apps/deploys/create';
import DeploySettings from './apps/deploys/settings';
import settings from './settings';
import QueryString from 'query-string'
import 'aui/src/js/umd/inline-dialog2';
import 'aui/src/js/umd/trigger';

var Link = Router.Link;
var Route = Router.Route;
var DefaultRoute = Router.DefaultRoute;
var RouteHandler = Router.RouteHandler;

var App = React.createClass({
  goToCreatePage: function (e) {
    this.context.router.transitionTo('create', {
      repoId: this.context.router.getCurrentParams().repoId
    }, QueryString.parse(location.search));
  },

  goToIndexPage: function (e) {
    this.context.router.transitionTo('index', {
      repoId: this.context.router.getCurrentParams().repoId
    }, QueryString.parse(location.search));
  },

  goToSettingsPage: function (e) {
    this.context.router.transitionTo('settings', {
      repoId: this.context.router.getCurrentParams().repoId
    }, QueryString.parse(location.search));
  },

  render: function() {
    return (
      <section id="content" className="main-content ac-content">
        <header className='AppHeader'>
          <div className='AppHeader-primary'>
            <img src={settings.STATIC_URL + 'svg/deployhub.svg'} />
          </div>
          <a onClick={this.goToCreatePage}>
            <button className='aui-button'>
              <span className='aui-icon aui-icon-small aui-iconfont-deploy'></span>
              <span>  Create</span>
            </button>
          </a>
          <a onClick={this.goToIndexPage}>
            <button className='aui-button'>
              <span className='aui-icon aui-icon-small aui-iconfont-editor-list-bullet'></span>
              <span>  View All</span>
            </button>
          </a>
          <a onClick={this.goToSettingsPage}>
            <button className='aui-button'>
              <span className='aui-icon aui-icon-small aui-iconfont-configure'></span>
              <span>  Settings</span>
            </button>
          </a>
        </header>

        <RouteHandler/>
      </section>
    )
  }
});

App.contextTypes = {
  router: React.PropTypes.func
};

var routes = (
  <Route name="app" path="/" handler={App}>
    <Route name="index" path="deploys/:repoId/overview" handler={DeployList}/>
    <Route name="create" path="deploys/:repoId/create" handler={DeployCreate}/>
    <Route name="settings" path="deploys/:repoId/settings" handler={DeploySettings}/>
    <DefaultRoute handler={DeployList}/>
  </Route>
);

Router.run(routes, Router.HistoryLocation, function (Handler) {
  React.render(<Handler/>, document.body);
});
