import React from 'react';

var AUISortableTable = React.createClass({
  getInitialState: function() {
    return {
      // TODO: sort by first col?
      order: 1
    }
  },

  genColumns: function () {
    return this.props.columns.map((c) => {
      return (
        <th onClick={this.sortBy} data-column-name={c.key}>{c.name}</th>
      );
    });
  },

  ensureColumns: function () {
    if (this.columnHtml) {
      return;
    }

    this.columnHtml = this.genColumns();
  },

  ensureSorting: function () {
    if (!this.state.lastSortColumn) {
      return;
    }
    var columnName = this.state.lastSortColumn;

    this.props.data.sort((a, b) => {
      if (a[columnName] > b[columnName]) {
        return 1 * this.state.order;
      } else if (a[columnName] === b[columnName]) {
        return 0;
      } else {
        return -1 * this.state.order;
      }
    });
  },

  render: function() {
    this.ensureColumns();
    this.ensureSorting();

    var rows = this.generateRows(this.props.data);

    return (
      <table className="aui aui-table-sortable">
        <thead>
          {this.columnHtml}
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
    );
  },

  generateRows: function(data) {
    return data.map(this.props.genRow);
  },

  /**
   *
   * @param {(1|-1)} order
   */
  sortBy: function(e) {
    var column = $(e.target).data('column-name');
    var lastColumn = this.state.lastSortColumn;

    if (lastColumn === column) {
      this.state.order *= -1;
    } else {
      this.state.order = 1;
    }

    this.setState({
      lastSortColumn: column,
      order: this.state.order
    });
  }
});

export default AUISortableTable;
