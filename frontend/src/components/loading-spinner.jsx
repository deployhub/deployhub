import './loading-spinner.css';
import React from 'react';

export default class LoadingSpinner extends React.Component {
  render() {
    return (
    <div className="Aligner full-screen">
      <div className="Aligner-item Aligner-item--top"></div>
      <div className="Aligner-item">
        <div className='triangles full-width'>
          <div className='tri invert'></div>
          <div className='tri invert'></div>
          <div className='tri'></div>
          <div className='tri invert'></div>
          <div className='tri invert'></div>
          <div className='tri'></div>
          <div className='tri invert'></div>
          <div className='tri'></div>
          <div className='tri invert'></div>
        </div>
      </div>
      <div className="Aligner-item Aligner-item--bottom"></div>
    </div>
    )
  }
}
