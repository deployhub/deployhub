import $ from 'jquery';

export default function validateHash(ownerId, repoId, hash) {
  var deferred = $.Deferred();

  AP.require('request', function(request){
    request({
      url: `/api/2.0/repositories/{${ownerId}}/{${repoId}}/commit/${hash}`,
      success: deferred.resolve,
      error: deferred.reject
    });
  });

  return deferred;
}