import forms from 'newforms';
import React from 'react';

/**
 * helper for new-forms plugin, to render fields according to ADG
 * @param {BoundField} bf
 * @returns {ReactComponent}
 */
export default function renderField(bf) {
  var className = 'field-group'
  if (bf.field instanceof forms.BooleanField) {
    return <div className={className}>
      <label>{bf.render()} {bf.label}</label>
      {bf.helpTextTag()} {bf.errors().render()}
    </div>
  }
  else {
    return <div className={className}>
      {bf.labelTag()} {bf.render()}
      {bf.helpTextTag()}
      <div className="error">
        {bf.errors().render()}
      </div>
    </div>
  }
}