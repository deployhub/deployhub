import forms from 'newforms';
import validateHash from '../utils/validateHash';

/**
 * temporary hack, since 'clean' does not seem to fire on the standard CharField
 */
var StringField = forms.CharField.extend({
  toJavaScript: function (val) {
    return val.trim();
  },
  validate: function (val) {
    if (!val) {
      throw forms.ValidationError("This field is required.");
    }
    return val;
  }
});

export default forms.Form.extend({
  hash: new StringField({
    widgetAttrs: {
      className: 'text'
    }
  }),

  description: new StringField({
    widget: forms.Textarea({
      attrs: {
        className: 'textarea'
      }
    })
  }),

  cleanHash: function (callback) {
    var repo = this.getRepo();
    validateHash(repo.owner.uuid, repo.uuid, this.cleanedData.hash).done(function () {
      // the callback must be invoked without arguments, which means we cannot
      // do `.done(callback)`
      callback();
    }).fail(function () {
      callback(null, forms.ValidationError("This sha does not exist at this repository."));
    });
  },

  /**
   * this seems to be called after all the other clean<Field> methods
   */
  clean: function () {
    this.forceReactUpdate();
  },

  constructor: function (kwargs) {
    forms.Form.call(this, kwargs);
    this.getRepo = kwargs.getRepo;
    this.forceReactUpdate = kwargs.forceReactUpdate;
  }
});
