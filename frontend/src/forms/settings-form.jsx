import forms from 'newforms';
import validateHash from '../utils/validateHash';

/**
 * temporary hack, since 'clean' does not seem to fire on the standard CharField
 */
var StringField = forms.CharField.extend({
  toJavaScript: function (val) {
    return val ? val.trim() : "";
  },
  validate: function (val) {
    if (!val) {
      throw forms.ValidationError("This field is required.");
    }
    return val;
  }
});

export default forms.Form.extend({
  root_hash: new StringField({
    widgetAttrs: {
      className: 'text'
    }
  }),

  /**
   * this seems to be called after all the other clean<Field> methods
   */
  clean: function () {
    this.forceReactUpdate();
  },

  clean_root_hash: function (callback) {
    // dummy code until proxy endpoint is in place
    validateHash(this.getRepo().uuid, this.cleanedData.root_hash).done(function () {
      callback();
    }).fail(function () {
      callback(null, forms.ValidationError("This sha does not exist at this repository."));
    });
  },

  constructor: function (kwargs) {
    forms.Form.call(this, kwargs);
    this.getRepo = kwargs.getRepo;
    this.forceReactUpdate = kwargs.forceReactUpdate;
  }
});
