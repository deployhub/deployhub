Deployhub is a Bitbucket connect add-on that helps track deployments.

# Caution #
While testing, you may notice issues while trying to connect to your local
machine's Bitbucket server. You may have to disable URL redirection in
Bitbucket's cname.py #process_request.

Also, if you are encountering unexpected redirects,
make sure that you have trailing slashes on API endpoints when
necessary.

# Installation #

1. Clone this repo (duh!)
2. Create a virtualenv and activate it
3. Run `pip install -r requirements.txt`
4. `cp deployhub/settings/dev_dist.py deployhub/settings/dev.py`
5. Run `DJANGO_SETTINGS_MODULE=deployhub.settings.dev ./manage.py migrate`
6. Run gunicorn using
  ```
  DJANGO_SETTINGS_MODULE=deployhub.settings.dev gunicorn
  --bind 0.0.0.0:8080 deployhub.wsgi:application
  ```
7. Run the frontend: `NODE_ENV=dev npm install && webpack -w`
8. Install the add-on into a locally running instance of Bitbucket by adding
`http://localhost:8080/connect/descriptor.json` to the _Custom add-ons_
section of a repository's settings.

# Using Docker #

Pre-reqs: [Docker Compose](https://docs.docker.com/compose/) and a way to run
Docker, possibly [boot2docker](http://boot2docker.io)

1. Get `docker-compose` configured to work correctly, maybe with
[these instructions](https://staging.bitbucket.org/bitbucket/bitbucket/src/dc49fb52a7f671787d37ebb9b143acdceb802a9f/readme.docker.md)
2. Run `docker-compose build` (you might need `sudo`) from within the repo
directory
3. If step 2 succeeded, then `docker-compose up` should start the web and
DB servers
4. With everything running, `docker-compose run web <command>` can run things
like `./manage.py` as needed
