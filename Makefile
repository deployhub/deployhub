deployhub/settings/dev.py: deployhub/settings/dev_dist.py
	cp deployhub/settings/dev_dist.py deployhub/settings/dev.py

install: requirements.txt deployhub/settings/dev.py
ifeq ("${VIRTUAL_ENV}", "")
	echo "Need virtualenv for install!"
else
	pip install -r requirements.txt
endif


up:
ifeq ("${VIRTUAL_ENV}", "")
	echo "Need virtualenv to run!"
else
	DJANGO_SETTINGS_MODULE=deployhub.settings.dev gunicorn --bind 0.0.0.0:8080 deployhub.wsgi:application
endif


.PHONY: install build up
