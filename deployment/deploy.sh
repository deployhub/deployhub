#!/bin/bash

# this script only works on linux, not OS X,
# because of the way core count is determined

pushd frontend
export NODE_ENV=production
npm install
npm install webpack -g
rm -rf build
cp -R static build
chown $DEPLOYHUB_USER build
make
popd

git pull origin master

mkdir -p $DEPLOYHUB_TMP_FOLDER
mkdir -p $DEPLOYHUB_MEDIA_FOLDER
mkdir -p $DEPLOYHUB_STATIC_FOLDER

$DEPLOYHUB_PIP install -r $DEPLOYHUB_APP_FOLDER/requirements.txt --upgrade

$DEPLOYHUB_PYTHON $DEPLOYHUB_APP_FOLDER/manage.py migrate
su - $DEPLOYHUB_USER -c "cd $DEPLOYHUB_APP_FOLDER && $DEPLOYHUB_PYTHON $DEPLOYHUB_APP_FOLDER/manage.py collectstatic --noinput"

worker_count=$(ps aux | grep deployhub.wsgi:application | wc -l)
kill -9 `ps -ef | grep deployhub.wsgi:application | grep -v grep | awk '{print $2}'`

while [ $worker_count -gt 1 ]; do
  sleep 0.5
  worker_count=$(ps aux | grep deployhub.wsgi:application | wc -l)
done

# "The number of worker processes. This number should generally be between 2-4 workers per core in the server."
# http://gunicorn-docs.readthedocs.org/en/latest/run.html
workers=$((2*$(cat /proc/cpuinfo | grep processor | wc -l)))
su - $DEPLOYHUB_USER -c "source $SALT_VARS && cd $DEPLOYHUB_APP_FOLDER && DEPLOYHUB_ENV=production gunicorn --workers=$workers --bind 0.0.0.0:$DEPLOYHUB_DJANGO_PORT deployhub.wsgi:application &"