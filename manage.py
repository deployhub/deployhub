#!/usr/bin/env python
import os
import sys

here = os.path.dirname(os.path.abspath(__file__))
root = os.path.join(here, '../')
sys.path.insert(0, root)

if __name__ == '__main__':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                          'deployhub.settings.deploy')
    from django.core.management import execute_from_command_line
    execute_from_command_line(sys.argv)
