FROM centos:7

# Install dependencies from the distro
RUN yum -y install epel-release
RUN yum -y install \
      gcc python devel python-devel libevent-devel python-pip \
      postgresql postgresql-devel \
      make


# Install Python dependencies
RUN pip install --upgrade pip

ADD . /code/deployhub
WORKDIR /code/deployhub

RUN pip install --no-deps -r requirements.txt

ENV DJANGO_SETTINGS_MODULE deployhub.settings.docker_dev

EXPOSE 8080
